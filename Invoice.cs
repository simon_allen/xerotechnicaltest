﻿using System;
using System.Collections.Generic;

namespace XeroTechnicalTest
{
    public class Invoice
    {
        public int InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }

        public List<InvoiceLine> LineItems { get; set; }

        public Invoice()
        {
            LineItems = new List<InvoiceLine>();
        }

        /// <summary>
        /// AddInvoiceLine should add a new invoice line
        /// </summary>
        /// <param name="invoiceLine">Invoice Line to add</param>
        public void AddInvoiceLine(InvoiceLine invoiceLine)
        {
            LineItems.Add(invoiceLine);
        }

        /// <summary>
        /// RemoveInvoiceLine should remove the invoice line with an InvoiceLineId matching someId
        /// </summary>
        /// <param name="someId">InvoiceLineId to remove</param>
        public void RemoveInvoiceLine(int someId)
        {
            // this could probably just be a Remove, not RemoveAll
            if(LineItems.RemoveAll(elem => elem.InvoiceLineId == someId) == 0)
            {
                throw new ArgumentOutOfRangeException("Couldn't find InvoiceLineId " + someId + " to remove");
            }

        }

        /// <summary>
        /// GetTotal should return the sum of (Cost * Quantity) for each line item
        /// </summary>
        public decimal GetTotal()
        {
            decimal total = 0;
            LineItems.ForEach(elem => total += elem.GetTotal());
            return total;
        }

        /// <summary>
        /// MergeInvoices appends the items from the sourceInvoice to the current invoice
        /// </summary>
        /// <param name="sourceInvoice">Invoice to merge from</param>
        public void MergeInvoices(Invoice sourceInvoice)
        {
            // we dont care if the invoice number or the invoice date are different
            sourceInvoice.LineItems.ForEach(sourceLine => {
                // first check if we have a matching InvoiceLineId already
                var matchingLine = LineItems.Find(line => line.InvoiceLineId == sourceLine.InvoiceLineId);
                if (matchingLine == null)
                {
                    // add it
                    AddInvoiceLine(sourceLine);
                } else {
                    // check cost is the same
                    if(sourceLine.Cost == matchingLine.Cost)
                    {
                        // merge it!
                        matchingLine.Quantity += sourceLine.Quantity;
                        // we dont care about the description!
                    } else
                    {
                        throw new InvalidOperationException("Can't merge invoice lines with matching InvoiceLineId but different Cost");
                    }
                }
            });
        }

        /// <summary>
        /// Creates a deep clone of the current invoice (all fields and properties)
        /// </summary>
        public Invoice Clone()
        {
            var clone = new Invoice();
            clone.InvoiceNumber = InvoiceNumber;
            clone.InvoiceDate = InvoiceDate;
            LineItems.ForEach(invoiceLine => clone.AddInvoiceLine(invoiceLine.Clone()));
            return clone;
        }

        /// <summary>
        /// Outputs string containing the following (replace [] with actual values):
        /// Invoice Number: [InvoiceNumber], InvoiceDate: [DD/MM/YYYY], LineItemCount: [Number of items in LineItems] 
        /// </summary>
        public override string ToString()
        {
            return String.Format("InvoiceNumber: {0}, InvoiceDate: {1}, LineItemCount: {2}", InvoiceNumber, InvoiceDate.ToString("dd/MM/yyyy"), LineItems.Count);
        }
    }
}