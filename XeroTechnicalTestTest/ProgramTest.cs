﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XeroTechnicalTest;

namespace XeroTechnicalTestTest
{
    [TestClass]
    public class ProgramTest
    {
        [TestMethod]
        public void CreateInvoiceWithOneItemTest()
        {
            var invoice = new Invoice();

            invoice.AddInvoiceLine(new InvoiceLine()
            {
                InvoiceLineId = 1,
                Cost = 6.99M,
                Quantity = 1,
                Description = "Apple"
            });

            Console.WriteLine(invoice.GetTotal());
            Assert.AreEqual(6.99M, invoice.GetTotal());
        }

        [TestMethod]
        public void CreateInvoiceWithMultipleItemsAndQuantities()
        {
            var invoice = new Invoice();

            invoice.AddInvoiceLine(new InvoiceLine()
            {
                InvoiceLineId = 1,
                Cost = 10.21M,
                Quantity = 4,
                Description = "Banana"
            });

            invoice.AddInvoiceLine(new InvoiceLine()
            {
                InvoiceLineId = 2,
                Cost = 5.21M,
                Quantity = 1,
                Description = "Orange"
            });

            invoice.AddInvoiceLine(new InvoiceLine()
            {
                InvoiceLineId = 3,
                Cost = 5.21M,
                Quantity = 5,
                Description = "Pineapple"
            });

            Console.WriteLine(invoice.GetTotal());
            Assert.AreEqual(72.1M, invoice.GetTotal());
        }

        [TestMethod]
        public void RemoveItem()
        {
            var invoice = new Invoice();

            invoice.AddInvoiceLine(new InvoiceLine()
            {
                InvoiceLineId = 1,
                Cost = 5.21M,
                Quantity = 1,
                Description = "Orange"
            });

            invoice.AddInvoiceLine(new InvoiceLine()
            {
                InvoiceLineId = 2,
                Cost = 10.99M,
                Quantity = 4,
                Description = "Banana"
            });

            // assuming this 1 is refrerring to InvoiceLineId = 1, not the 1(th) element in the zero based array
            invoice.RemoveInvoiceLine(1);
            Console.WriteLine(invoice.GetTotal());

            Assert.AreEqual(43.96M, invoice.GetTotal());
        }

        [TestMethod]
        public void MergeInvoices()
        {
            var invoice1 = new Invoice();

            invoice1.AddInvoiceLine(new InvoiceLine()
            {
                InvoiceLineId = 1,
                Cost = 10.33M,
                Quantity = 4,
                Description = "Banana"
            });

            var invoice2 = new Invoice();

            invoice2.AddInvoiceLine(new InvoiceLine()
            {
                InvoiceLineId = 2,
                Cost = 5.22M,
                Quantity = 1,
                Description = "Orange"
            });

            invoice2.AddInvoiceLine(new InvoiceLine()
            {
                InvoiceLineId = 3,
                Cost = 6.27M,
                Quantity = 3,
                Description = "Blueberries"
            });

            invoice1.MergeInvoices(invoice2);
            Console.WriteLine(invoice1.GetTotal());

            Assert.AreEqual(65.35M, invoice1.GetTotal());
        }

        [TestMethod]
        public void CloneInvoice()
        {
            var invoice = new Invoice();

            invoice.AddInvoiceLine(new InvoiceLine()
            {
                InvoiceLineId = 1,
                Cost = 6.99M,
                Quantity = 1,
                Description = "Apple"
            });

            invoice.AddInvoiceLine(new InvoiceLine()
            {
                InvoiceLineId = 2,
                Cost = 6.27M,
                Quantity = 3,
                Description = "Blueberries"
            });

            var clonedInvoice = invoice.Clone();
            Console.WriteLine(clonedInvoice.GetTotal());
            Assert.AreEqual(25.8M, clonedInvoice.GetTotal());
        }

        [TestMethod]
        public void InvoiceToString()
        {
            var now = DateTime.Now;
            var invoice = new Invoice()
            {
                InvoiceDate = now,
                InvoiceNumber = 1000,
                LineItems = new List<InvoiceLine>()
                {
                    new InvoiceLine()
                    {
                        InvoiceLineId = 1,
                        Cost = 6.99M,
                        Quantity = 1,
                        Description = "Apple"
                    }
                }
            };

            Console.WriteLine(invoice.ToString());
            //Invoice Number: [InvoiceNumber], InvoiceDate: [DD/MM/YYYY], LineItemCount: [Number of items in LineItems] 
            // a regular expression would be better here...
            // potential for this check to fail due to the date changing between the DateTime.Now above and the DateTime.Now below...
            //  okay... I'll fix it then...
            Assert.AreEqual("InvoiceNumber: 1000, InvoiceDate: " + now.ToString("dd/MM/yyyy") + ", LineItemCount: 1", invoice.ToString());
        }

    }
}
