using System;
using XeroTechnicalTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace XeroTechnicalTestTest
{
    [TestClass]
    public class InvoiceTest
    {
        [TestMethod]
        public void TestAddInvoiceLine()
        {
            var invoice = CreateBaseInvoice();
            Assert.AreEqual(0, invoice.LineItems.Count, "should be no line items to start with");
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine());
            Assert.AreEqual(1, invoice.LineItems.Count, "should be 1 line item now");
            var line1 = invoice.LineItems[0];
            Assert.AreEqual("Blah:1", line1.Description, "Line 1 description should be Blah:1");
        }

        [TestMethod]
        public void TestRemoveInvoiceLineBasic()
        {
            var invoice = CreateBaseInvoice();
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(345));
            Assert.AreEqual(1, invoice.LineItems.Count, "should be 1 line item now");
            invoice.RemoveInvoiceLine(345);
            Assert.AreEqual(0, invoice.LineItems.Count, "should be no line items now");
        }

        [TestMethod]
        public void TestRemoveInvoiceLineMultipleLines()
        {
            var invoice = CreateBaseInvoice();
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(1));
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(2));
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(3));
            Assert.AreEqual(3, invoice.LineItems.Count, "should be 3 line items now");
            invoice.RemoveInvoiceLine(2);
            Assert.AreEqual(2, invoice.LineItems.Count, "should be 2 line items now");
            invoice.RemoveInvoiceLine(1);
            Assert.AreEqual(1, invoice.LineItems.Count, "should be 1 line item now");
            invoice.RemoveInvoiceLine(3);
            Assert.AreEqual(0, invoice.LineItems.Count, "should be 0 line items now");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestRemoveInvoiceInvalidId()
        {
            var invoice = CreateBaseInvoice();
            invoice.RemoveInvoiceLine(18);
        }

        /* I wrote this method assuming the LineItemId was the same as the index in the Invoice LineItems List
         *  but, decided that didn't make much sense so this whole test isn't valid!
        [TestMethod]
        public void TestRemoveInvoiceLineRenumberLineItemId()
        {
            var invoice = CreateBaseInvoice();
            invoice.AddInvoiceLine(CreateBaseInvoiceLine(1));
            invoice.AddInvoiceLine(CreateBaseInvoiceLine(2));
            invoice.AddInvoiceLine(CreateBaseInvoiceLine(3));
            Assert.AreEqual(3, invoice.LineItems.Count, "should be 3 line items now");
            invoice.RemoveInvoiceLine(1);
            Assert.AreEqual(2, invoice.LineItems.Count, "should be 2 line items now");
            // verify InvoiceLineId is still sequential - no gaps
            var line1 = invoice.LineItems[0];
            var line2 = invoice.LineItems[1];
            Assert.AreEqual(1, line1.InvoiceLineId, "First Line Item LineId should be 1");
            Assert.AreEqual("Blah:1", line1.Description, "First Line Item description should be Blah:1");

            Assert.AreEqual(2, line2.InvoiceLineId, "Second Line Item LineId should be 2");
            Assert.AreEqual("Blah:3", line1.Description, "Second Line Item description should be Blah:3");
        }*/

        [TestMethod]
        public void TestGetTotal1()
        {
            var invoice = CreateBaseInvoice();
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(7));
            Assert.AreEqual(49, invoice.GetTotal(), "total should be $49 - 7 items x $7 each");
        }

        [TestMethod]
        public void TestGetTotal2()
        {
            var invoice = CreateBaseInvoice();
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(7));
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(3));
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(8));
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(1));
            Assert.AreEqual(123, invoice.GetTotal(), "total should be $123 - $49(7) + $9(3) + $64(8) + $1 ");
        }

        [TestMethod]
        public void TestMergeInvoicesBasic()
        {
            var invoice1 = CreateBaseInvoice(12345);
            var invoice2 = CreateBaseInvoice(12346);
            invoice1.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(5));
            invoice2.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(9));

            invoice1.MergeInvoices(invoice2);

            Assert.AreEqual(2, invoice1.LineItems.Count, "Merged invoice should have 2 line items");
            Assert.AreEqual("Blah:5", invoice1.LineItems[0].Description, "Line 1 description should be Blah:5");
            Assert.AreEqual("Blah:9", invoice1.LineItems[1].Description, "Line 2 description should be Blah:9");
        }

        [TestMethod]
        public void TestMergeInvoicesMatchingIdAndCost()
        {
            var invoice1 = CreateBaseInvoice(12345);
            var invoice2 = CreateBaseInvoice(12346);
            invoice1.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(5));
            invoice2.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(5));

            invoice1.MergeInvoices(invoice2);

            Assert.AreEqual(1, invoice1.LineItems.Count, "Merged invoice should have 1 line item");
            Assert.AreEqual("Blah:5", invoice1.LineItems[0].Description, "Line 1 description should be Blah:5");
            Assert.AreEqual(10, invoice1.LineItems[0].Quantity, "Line 1 quantity should be 10 (5 + 5)");
            Assert.AreEqual(5, invoice1.LineItems[0].Cost, "Line 1 quantity should be 10 (5 + 5)");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        // merge must fail if ids are matching but the cost is different
        public void TestMergeInvoicesMatchingIdDifferentCost()
        {
            var invoice1 = CreateBaseInvoice(12345);
            var invoice2 = CreateBaseInvoice(12346);
            invoice1.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(5));
            var invoiceLine2 = InvoiceLineTest.CreateBaseInvoiceLine(5);
            invoiceLine2.Cost = 8.4M;
            invoice2.AddInvoiceLine(invoiceLine2);

            invoice1.MergeInvoices(invoice2);
        }

        [TestMethod]
        public void TestClone()
        {
            var invoice1 = CreateBaseInvoice(12345);
            invoice1.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(7));
            invoice1.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(6));
            var invoice2 = invoice1.Clone();
            Assert.AreEqual(12345, invoice2.InvoiceNumber, "cloned invoice should have invoice number 12345");
            Assert.AreEqual(2, invoice2.LineItems.Count, "cloned invoice should have 2 line items");
            // these tests should probably go in InvoiceLineTest
            Assert.AreEqual("Blah:7", invoice1.LineItems[0].Description, "Line 1 description should be Blah:7");
            Assert.AreEqual("Blah:6", invoice1.LineItems[1].Description, "Line 2 description should be Blah:6");
        }

        [TestMethod]
        public void TestToString()
        {
            var invoice = CreateBaseInvoice(987, 2019, 09, 11);
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(3));
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(4));
            invoice.AddInvoiceLine(InvoiceLineTest.CreateBaseInvoiceLine(5));

            var toStr = invoice.ToString();
            Console.WriteLine(toStr);
            Assert.IsTrue(toStr.Contains("987"), "string output must contain invoice number 987");
            Assert.IsTrue(toStr.Contains("11/09/2019"), "string output must contain invoice date 11/09/2019");
        }

        /// <summary>
        /// CreateBaseInvoice creates a simple invoice object
        /// </summary>
        private Invoice CreateBaseInvoice(int id = 12345, int yy = 2019, int mm = 10, int dd = 15)
        {
            return new Invoice()
            {
                InvoiceNumber = id,
                InvoiceDate = new DateTime(yy, mm, dd)
            };
        }


    }
}
