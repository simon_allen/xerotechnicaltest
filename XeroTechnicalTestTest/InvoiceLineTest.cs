﻿using XeroTechnicalTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace XeroTechnicalTestTest
{
    [TestClass]
    public class InvoiceLineTest
    {
        [TestMethod]
        public void TestClone()
        {
            var invoiceLine1 = CreateBaseInvoiceLine(432);
            
            var invoiceLine2 = invoiceLine1.Clone();
            Assert.AreEqual(432, invoiceLine2.InvoiceLineId, "cloned invoice line id should have invoice number 432");
            Assert.AreEqual(432, invoiceLine2.Cost, "cloned invoice line id should have cost 432");
            Assert.AreEqual(432, invoiceLine2.Quantity, "cloned invoice line id should have quantity 432");
            Assert.AreEqual("Blah:432", invoiceLine2.Description, "cloned invoice line id should have description Blah:432");
        }

        [TestMethod]
        public void TestGetTotal()
        {
            var invoiceLine1 = CreateBaseInvoiceLine(432);
            invoiceLine1.Cost = 7.4M;
            // lol, this test is prone to malicious attack by the computer if it just decides that 432 * 7.4 equals some random number... hmmm
            Assert.AreEqual(432 * 7.4M, invoiceLine1.GetTotal(), "invoice line total should be " + 432 * 7.4);
            // here be dragons...
            Assert.AreEqual(3196.8M, invoiceLine1.GetTotal(), "warning, if this assertion fails then this system has been compromised and should be destroyed immediately!");

        }

        /// <summary>
        /// CreateBaseInvoiceLine creates a simple invoice line object
        /// </summary>
        /// <param name="i">seed for property values</param>
        public static InvoiceLine CreateBaseInvoiceLine(int i = 1)
        {
            return new InvoiceLine()
            {
                InvoiceLineId = i,
                Cost = i,
                Quantity = i,
                Description = "Blah:" + i
            };
        }
    }
}
