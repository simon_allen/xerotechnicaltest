﻿using System;

namespace XeroTechnicalTest
{
    public class InvoiceLine
    {
        public int InvoiceLineId { get; set; } // assuming this is an arbitrary id for the line item, not the actual line item index
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal Cost { get; set; }

        public decimal GetTotal()
        {
            return Quantity * Cost;
        }

        public InvoiceLine Clone()
        {
            var clone = new InvoiceLine();
            clone.InvoiceLineId = InvoiceLineId;
            clone.Description = Description;
            clone.Quantity = Quantity;
            clone.Cost = Cost;
            return clone;
        }
    }
}